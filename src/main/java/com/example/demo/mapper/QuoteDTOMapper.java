package com.example.demo.mapper;

import com.example.demo.dto.QuoteDTO;
import com.example.demo.dto.UserCreateDTO;
import com.example.demo.entity.QuoteEntity;
import com.example.demo.entity.UserEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class QuoteDTOMapper {
    private final ModelMapper modelMapper;

    public QuoteDTOMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public QuoteEntity toEntity(QuoteDTO quoteDTO){
        return modelMapper.map(quoteDTO , QuoteEntity.class);
    }
}
