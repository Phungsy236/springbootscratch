package com.example.demo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class ErrorValidateDTO {
    String subjectName;
    Map<String , String> errorDetailList;
}
