package com.example.demo.service;

import com.example.demo.dto.QuoteDTO;
import com.example.demo.entity.QuoteEntity;
import com.example.demo.mapper.QuoteDTOMapper;
import com.example.demo.repository.QuoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class GreetingService {
    @Autowired
    QuoteRepository quoteRepository;

    @Autowired
    QuoteDTOMapper quoteDTOMapper;
    public void cloneQuote(){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<QuoteDTO>> listQuote =  restTemplate.exchange("https://api.quotable.io/quotes/random?limit=5",
                HttpMethod.GET, null,
                new ParameterizedTypeReference<List<QuoteDTO>>() {
                });
        List<QuoteDTO> quoteList = listQuote.getBody();
        List<QuoteEntity> quoteEntityList = new ArrayList<>();
        quoteList.forEach(quoteDTO -> {
            QuoteEntity quoteEntity = quoteDTOMapper.toEntity(quoteDTO);
            quoteEntityList.add(quoteEntity);
        });
        quoteRepository.saveAll(quoteEntityList);

    }
}
