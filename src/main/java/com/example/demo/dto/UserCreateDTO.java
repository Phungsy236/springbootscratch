package com.example.demo.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateDTO {
    @NotNull(message = "Name is required")
    String name;

    @NotNull(message = "Email is required")
    @Email(message = "Invalid email")
    String email;

    @Length(min = 6, max = 12, message = "Password must between 6 and 12 character")
    String password;
}
