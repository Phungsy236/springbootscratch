package com.example.demo.controller;

import com.example.demo.dto.GreetingModel;
import com.example.demo.dto.QuoteDTO;
import com.example.demo.entity.QuoteEntity;
import com.example.demo.mapper.QuoteDTOMapper;
import com.example.demo.repository.QuoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/greeting")
public class GreetingController {

    @GetMapping(params = "msg")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public GreetingModel sendHello(@RequestParam(name = "msg" , defaultValue = "greeting msg") String msg){
        var greeting = new GreetingModel(1 , msg);
        return greeting;
    }

}
