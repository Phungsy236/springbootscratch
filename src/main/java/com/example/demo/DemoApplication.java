package com.example.demo;

import com.example.demo.service.GreetingService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableScheduling
public class DemoApplication {
	@Autowired
	@Lazy
	GreetingService greetingService;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper () {
		return new ModelMapper();
	}

	@Scheduled( timeUnit = TimeUnit.MINUTES, fixedRate = 30 , initialDelay = 1)
	@Async
	void crawQuoteData(){
		System.out.println("=================== Start scheduling task ====================");
		greetingService.cloneQuote();
		System.out.println("=================== End scheduling task ====================");
	}

}
