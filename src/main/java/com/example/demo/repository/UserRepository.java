package com.example.demo.repository;

import com.example.demo.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<UserEntity , Integer>,CrudRepository<UserEntity,Integer>{
    UserEntity findByName(String username);
    Optional<UserEntity> findByNameAndPassword(String name , String password);
}
