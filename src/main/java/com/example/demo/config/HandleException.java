package com.example.demo.config;

import com.example.demo.dto.ErrorValidateDTO;
import jakarta.validation.ValidationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.nio.file.AccessDeniedException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestControllerAdvice
public class HandleException {

    @ExceptionHandler({ RuntimeException.class })
    public ResponseEntity<Object> handleRuntime(
            Exception ex, WebRequest request) {
        return new ResponseEntity<Object>(
                Map.of("error" , ex.getMessage())
                , new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ AccessDeniedException.class })
    public ResponseEntity<Object> handleAccessDeniedException(
            Exception ex, WebRequest request) {
        return new ResponseEntity<Object>(
                "Access denied message here", new HttpHeaders(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        ErrorValidateDTO errorResponse = new ErrorValidateDTO();
        errorResponse.setSubjectName(ex.getObjectName());
        Map<String , String> tmp = new HashMap<>();
        for ( var error : ex.getFieldErrors()){
            tmp.put(error.getField(),error.getDefaultMessage());
        }
        errorResponse.setErrorDetailList(tmp);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({SQLIntegrityConstraintViolationException.class})
    public ResponseEntity<Object> handleSQLIntegrityConstraintViolationException(SQLIntegrityConstraintViolationException e ){
        return new ResponseEntity<>(Map.of("SQLError",e.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
