package com.example.demo.mapper;

import com.example.demo.dto.UserCreateDTO;
import com.example.demo.entity.UserEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class UserDTOMapper {
    private final ModelMapper modelMapper;

    public UserDTOMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public UserEntity toEntity(UserCreateDTO userCreateDTO){
        return modelMapper.map(userCreateDTO , UserEntity.class);
    }
//    public
}
