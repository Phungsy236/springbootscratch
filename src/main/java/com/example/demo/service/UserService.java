package com.example.demo.service;

import com.example.demo.dto.LoginDTO;
import com.example.demo.dto.LoginedUser;
import com.example.demo.dto.UserCreateDTO;
import com.example.demo.entity.UserEntity;
import com.example.demo.mapper.UserDTOMapper;
import com.example.demo.repository.UserRepository;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final UserDTOMapper userDTOMapper;
    private final JwtService jwtService;

    public UserService(UserRepository userRepository, UserDTOMapper userDTOMapper , @Lazy JwtService jwtService) {
        this.userRepository = userRepository;
        this.userDTOMapper = userDTOMapper;
        this.jwtService = jwtService;

    }

    public Page<UserEntity> getAll(int page , int size , String sortName){
        return userRepository.findAll(PageRequest.of(page,size , Sort.by(sortName).descending()));
    }

    public byte[] printDataToExcel() throws IOException {
        Iterable<UserEntity> entityList = userRepository.findAll();
        ExcelService excelService = new ExcelService((List<UserEntity>) entityList);
        return excelService.generateExcelFile();
    }

    public UserEntity saveUser(UserCreateDTO userCreateDTO){
        UserEntity userEntity = userDTOMapper.toEntity(userCreateDTO);
        return userRepository.save(userEntity);
    }
    public UserEntity updateUser(int id , UserCreateDTO userCreateDTO){
        UserEntity userEntity = userRepository.findById(id).orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND ));
        BeanUtils.copyProperties(userCreateDTO , userEntity);
        return userRepository.save(userEntity);
    }
    public UserEntity getDetail(int id){
        UserEntity userEntity = userRepository.findById(id).orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND ));
        return userEntity;
    }
    public boolean deleteUser(int id){
        UserEntity userEntity = userRepository.findById(id).orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND ));
        try{
            userRepository.delete(userEntity);
            return true;
        }catch (Exception e){
            System.out.println(e);
            return false;
        }
    }
    public LoginedUser login(LoginDTO loginDTO)  {
        UserEntity userEntity = userRepository.findByNameAndPassword(loginDTO.getUsername() , loginDTO.getPassword()).orElseThrow(
                ()->  new RuntimeException("sai mk")
        );
        String token = jwtService.generateToken(userEntity);
        LoginedUser user =  new LoginedUser(token , userEntity);
        return user;
    }

}

