package com.example.demo.repository;

import com.example.demo.entity.QuoteEntity;
import com.example.demo.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface QuoteRepository extends CrudRepository<QuoteEntity,Integer> {
}
