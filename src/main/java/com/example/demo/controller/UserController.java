package com.example.demo.controller;

import com.example.demo.dto.LoginDTO;
import com.example.demo.dto.LoginedUser;
import com.example.demo.dto.UserCreateDTO;
import com.example.demo.entity.UserEntity;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping(params = {"page","size","sortName"} , path = "/paginate")
    Page<UserEntity> getAllUser(@RequestParam(name = "page") int page , @RequestParam(name="size") int size , @RequestParam(name="sortName") String sortName ){
        return userService.getAll(page , size , sortName);
    }

    @PostMapping()
    UserEntity saveUser(@Valid @RequestBody UserCreateDTO userCreateDTO){
        return userService.saveUser(userCreateDTO);
    }
    @PatchMapping("/{id}")
    UserEntity updateUser(@PathVariable int id, @RequestBody UserCreateDTO userCreateDTO){
        return userService.updateUser(id  , userCreateDTO);
    }
    @GetMapping("/{id}")
    UserEntity getDetailUser(@PathVariable int id){
        return userService.getDetail(id);
    }
    @DeleteMapping("/{id}")
    boolean deleteUser(@PathVariable int id){
        return userService.deleteUser(id);
    }
    @GetMapping("/export-to-excel")
    public ResponseEntity<Resource> exportIntoExcelFile() throws IOException {
        Resource resource = new ByteArrayResource(userService.printDataToExcel());
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=report.xls")
                .body(resource);
    }
    @PostMapping("/login")
    LoginedUser login(@RequestBody LoginDTO loginDTO) {
        return userService.login(loginDTO);
    }
}
