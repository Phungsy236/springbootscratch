package com.example.demo.dto;

public record GreetingModel(long id , String message) { }
